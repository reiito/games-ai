﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockManager : MonoBehaviour
{

  public static int flockSize = 25;
  public GameObject sheepPrefab;
  public GameObject sheepSpawn;
  public static GameObject[] allFlock = new GameObject[flockSize];
  public static int startRoom = 30;

  // Use this for initialization
  void Start()
  {
    int i = 0;
    for (int x = 0; x < Mathf.Sqrt(flockSize); x++)
    {
      for (int y = 0; y < Mathf.Sqrt(flockSize); y++)
      {
        Vector3 pos = new Vector3(sheepSpawn.transform.position.x + x * 2.0f,
                        0,
                        sheepSpawn.transform.position.z + y * 2.0f);
        allFlock[i] = (GameObject)Instantiate(sheepPrefab, pos, Quaternion.identity, transform);
        allFlock[i].AddComponent<Rigidbody>();
        allFlock[i].AddComponent<BoxCollider>();
        allFlock[i].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY;
        allFlock[i].GetComponent<Rigidbody>().isKinematic = true;
        i++;
      }
    }
  }

}
