﻿using System.Collections.Generic;
using UnityEngine;

public class LevelGrid : MonoBehaviour
{
  public bool visualDebug;

  public LayerMask unwalkableMask; //filter terrain
  public Vector2 gridWorldSize; //area the grid covers
  public float nodeRadius; //node space

  public int MaxSize { get { return gridSizeX * gridSizeY; } }

  public TerrainType[] walkableRegions;
  public int obstacleProximityPenalty = 10;

  LayerMask walkableMask;
  Dictionary<int, int> walkableRegionsDictionary = new Dictionary<int, int>();

  Node[,] grid; //level nodes
  float nodeDiameter;
  int gridSizeX, gridSizeY;

  int penaltyMin = int.MaxValue;
  int penaltyMax = int.MinValue;

  const float GIZMO_CUBE_PADDING = 0.1f;

  // MonoBehaviour functions

  private void Awake()
  {
    nodeDiameter = nodeRadius * 2; //get node total size
    // how many nodes can fit on the x & y axis
    gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
    gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);

    // 
    foreach (TerrainType region in walkableRegions)
    {
      walkableMask.value |= region.terrainMask.value;
      walkableRegionsDictionary.Add((int)Mathf.Log(region.terrainMask.value, 2), region.terrainPenalty);
    }

    CreateGrid();
  }

  private void OnDrawGizmos()
  {
    Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, 1, gridWorldSize.y)); //draw debug cubes to show grid
    if (grid != null && visualDebug)
    {
      foreach (Node n in grid)
      {
        // display pathfinding arrays, path and unwalkables/weights
        if (n.inList == IN.PATH)
          Gizmos.color = Color.blue;
        else if (n.inList == IN.OPEN)
          Gizmos.color = Color.green;
        else if (n.inList == IN.CLOSED)
          Gizmos.color = Color.red;
        else
        {
          Gizmos.color = Color.Lerp(Color.white, Color.black, Mathf.InverseLerp(penaltyMin, penaltyMax, n.movementPenalty));
          Gizmos.color = (n.walkable) ? Gizmos.color : Color.black;
        }
        Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter - GIZMO_CUBE_PADDING)); //create (0.9,0.9,0.9) cube (smaller for padding)
      }
    }
  }


  // Non-Mono functions

  //
  void CreateGrid()
  {
    grid = new Node[gridSizeX, gridSizeY]; //create appropriate sized grid
    Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 //left edge
      - Vector3.forward * gridWorldSize.y / 2; //bottom edge

    // fill grid
    for (int x = 0; x < gridSizeX; x++)
    {
      for (int y = 0; y < gridSizeY; y++)
      {
        Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) // place x points
          + Vector3.forward * (y * nodeDiameter + nodeRadius); // place y points
        bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask)); //check for obstacles
        int movementPenalty = 0;

        Ray ray = new Ray(worldPoint + Vector3.up * 50, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100, walkableMask))
          walkableRegionsDictionary.TryGetValue(hit.collider.gameObject.layer, out movementPenalty);

        if (!walkable)
          movementPenalty += obstacleProximityPenalty;

        grid[x, y] = new Node(walkable, worldPoint, x, y, movementPenalty, IN.NONE); //add node to point
      }
    }

    BlurPenaltyMap(5);
  }

  //
  void BlurPenaltyMap(int blurSize)
  {
    int kernelSize = blurSize * 2 + 1;
    int kernelExtents = (kernelSize - 1) / 2;

    int[,] penatliesHorizontalPass = new int[gridSizeX, gridSizeY];
    int[,] penatliesVerticalPass = new int[gridSizeX, gridSizeY];

    for (int y = 0; y < gridSizeY; y++)
    {
      for (int x = -kernelExtents; x <= kernelExtents; x++)
      {
        int sampleX = Mathf.Clamp(x, 0, kernelExtents);
        penatliesHorizontalPass[0, y] += grid[sampleX, y].movementPenalty;
      }

      for (int x = 1; x < gridSizeX; x++)
      {
        int removeIndex = Mathf.Clamp(x - kernelExtents - 1, 0, gridSizeX);
        int addIndex = Mathf.Clamp(x + kernelExtents, 0, gridSizeX - 1);

        penatliesHorizontalPass[x, y] = penatliesHorizontalPass[x - 1, y] - grid[removeIndex, y].movementPenalty + grid[addIndex, y].movementPenalty;
      }
    }

    for (int x = 0; x < gridSizeX; x++)
    {
      for (int y = -kernelExtents; y <= kernelExtents; y++)
      {
        int sampleY = Mathf.Clamp(y, 0, kernelExtents);
        penatliesVerticalPass[x, 0] += penatliesHorizontalPass[x, sampleY];
      }

      int blurredPenatly = Mathf.RoundToInt((float)penatliesVerticalPass[x, 0] / (kernelSize * kernelSize));
      grid[x, 0].movementPenalty = blurredPenatly;

      for (int y = 1; y < gridSizeY; y++)
      {
        int removeIndex = Mathf.Clamp(y - kernelExtents - 1, 0, gridSizeY);
        int addIndex = Mathf.Clamp(y + kernelExtents, 0, gridSizeY - 1);

        penatliesVerticalPass[x, y] = penatliesVerticalPass[x, y - 1] - penatliesHorizontalPass[x, removeIndex] + penatliesHorizontalPass[x, addIndex];
        blurredPenatly = Mathf.RoundToInt((float)penatliesVerticalPass[x, y] / (kernelSize * kernelSize));
        grid[x, y].movementPenalty = blurredPenatly;

        if (blurredPenatly > penaltyMax)
          penaltyMax = blurredPenatly;

        if (blurredPenatly < penaltyMin)
          penaltyMin = blurredPenatly;
      }
    }
  }


  // get node depending on given position
  public Node NodeFromWorldPoint(Vector3 worldPosition)
  {
    // get how far the position is on the grid
    float percentX = (worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x;
    float percentY = (worldPosition.z + gridWorldSize.y / 2) / gridWorldSize.y;
    percentX = Mathf.Clamp01(percentX);
    percentY = Mathf.Clamp01(percentY);

    // get grid array location
    int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
    int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);

    return grid[x, y];
  }

  // get a given node's surrounding nodes (neighbours)
  public List<Node> GetNeighbours(Node node)
  {
    List<Node> neighbours = new List<Node>();

    // search in a 3x3 block
    for (int x = -1; x <= 1; x++)
    {
      for (int y = -1; y <= 1; y++)
      {
        if (x == 0 && y == 0) //skip center (current node)
          continue;

        // grab neighbouring node
        int checkX = node.gridX + x;
        int checkY = node.gridY + y;

        // check if node is within the grid
        if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
          neighbours.Add(grid[checkX, checkY]);
      }
    }

    return neighbours;
  }

  // terrain class for terrain details
  [System.Serializable]
  public class TerrainType
  {
    public LayerMask terrainMask;
    public int terrainPenalty;
  }
}
