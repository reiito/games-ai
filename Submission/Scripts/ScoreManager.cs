﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
  public Text scoreText;

  public int score;
  public int scoreValue;
  public int initialSheep;
  public int sheepLeft;
  float timeLeft;
  public Text timeText;
  public Text endText;
  public static bool end;
  private bool displayOnce;

  public GameObject endMenuUI;


  private void Start()
  {
    Time.timeScale = 1f;
    score = 0;
    scoreValue = 5;
    timeLeft = 240.0f;
    initialSheep = FlockManager.flockSize;
    sheepLeft = FlockManager.flockSize;
    end = false;
    displayOnce = true;
    SetScoreText();
  }

  private void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.CompareTag("Sheep"))
    {
      //Destroy(other.gameObject);
      score += scoreValue;
      SetScoreText();
      minusSheep();
      //Teleport Sheep
      other.gameObject.GetComponent<SheepBehavior>().win = true;
    }
  }

  public void minusScore()
  {
    score -= scoreValue;
  }

  public void minusSheep()
  {
    sheepLeft -= 1;
  }

  public void SetScoreText()
  {
    scoreText.text = "Score: " + score.ToString();
  }

  void SetTimeText()
  {
    timeText.text = "Time Left: " + Mathf.Round(timeLeft).ToString(); ;
  }

  void Update()
  {
    if (timeLeft >= 0)
    {
      timeLeft -= Time.deltaTime;
    }

    if (timeLeft <= 0f || sheepLeft <= 0)
    {
      end = true;
      if (displayOnce)
      {
        displayOnce = false;
        endGame();
        winCheck();
      }

    }
    else
    {
      SetTimeText();
    }
  }

  void winCheck()
  {
    //>80%
    if (score >= ((initialSheep * .8) * scoreValue))
    {
      endText.text = "Congratulations! You guided the number of sheep needed for a successful breeding season! You win with a score of " + score.ToString();
    }
    //<20%
    else if (score < ((initialSheep * .2) * scoreValue))
    {
      endText.text = "Sorry! You lost too many sheep for the breeding season, better luck next time!";
    }
    //All Sheep Gone
    else if (sheepLeft <= 0)
    {
      //>80%
      if (score >= ((initialSheep * .8) * scoreValue))
      {
        endText.text = "Congratulations! You guided the number of sheep needed for a successful breeding season! You win with a score of " + score.ToString();
      }
      //>65%
      else if (score >= ((initialSheep * .65) * scoreValue))
      {
        endText.text = "Well done! You guided just enough sheep needed for a good breeding season! You win with a score of " + score.ToString();
      }
      //<65%
      else if (score < ((initialSheep * .65) * scoreValue))
      {
        endText.text = "Sorry! You lost too many sheep for the breeding season, better luck next time!";
      }
    }
  }

  void endGame()
  {
    Time.timeScale = 0f;
    endMenuUI.SetActive(true);
  }
}
