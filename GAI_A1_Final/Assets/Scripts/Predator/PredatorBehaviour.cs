﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredatorBehaviour : StateMachine
{
  const float minPathUpdateTime = 0.5f;
  const float pathUpdateMoveThreshold = 1.5f;

  public float speed = 10.0f;
  public float turnSpeed = 3;
  public float turnDistance = 5;
  public float stoppingDistance = 0;
  public float chaseDistance = 50;

  ScoreManager scoreManager;
  Path path;
  LevelGrid levelGrid;

  Transform preyTarget;
  Transform preyTargetPig;
  float distanceToPrey = 1000;
  Transform fleeTarget;

  Vector2 levelLimit;

  // state switches
  bool roaming = false;
  bool chasing = false;
  bool fleeing = false;
  bool coroutineStarted = false;

  private void Start()
  {
    levelGrid = GameObject.Find("PathFindingManager").GetComponent<LevelGrid>();
    fleeTarget = GameObject.Find("FleeTarget").GetComponent<Transform>();
    scoreManager = GameObject.Find("PenCollider").GetComponent<ScoreManager>();
    levelLimit = levelGrid.gridWorldSize / 2;
    SetDefaultstate(Idle);

    AddStateAndCondition(Roam, IsRoaming);
    AddStateAndCondition(Chase, IsChasing);
    AddStateAndCondition(Flee, IsFleeing);

    // start roaming
    roaming = true;
    RandomTarget();
    coroutineStarted = true;
  }

  void Idle()
  {
    StopAllCoroutines();
  }

  void Roam()
  {
    //maybe put in to path follow random positions around the map
    if (coroutineStarted)
    {
      StopAllCoroutines();
      coroutineStarted = false;
      StartCoroutine(UpdatePath(fleeTarget));
    }

    GetClosestPrey();

    if (distanceToPrey < chaseDistance)
    {
      roaming = false;
      chasing = true;
      coroutineStarted = true;
    }
  }

  bool IsRoaming()
  {
    // coroutine to roam for a couple of seconds then chase OR roam until distance to sheep is low enough to warrant a chase;
    if (roaming)
      return true;
    else
      return false;
  }

  void Chase()
  {
    if (coroutineStarted)
    {
      StopAllCoroutines();
      coroutineStarted = false;
      // 60% chase sheep, 40% chase pigs (pigs worth more points)
      float randNum = Random.Range(0.0f, 1.0f);
      if (randNum >= 0.6f)
      {
        StartCoroutine(UpdatePath(preyTarget));
      }
      else
      {
        StartCoroutine(UpdatePath(preyTargetPig));
      }
      
    }

    GetClosestPrey();
  }

  bool IsChasing()
  {
    // set to true if sheep are in proximity OR at a random time
    if (chasing)
      return true;
    else
      return false;
  }

  void Flee()
  {
    // implement fleeing steering behaviour (go the opposite direction for a certain amount)
    if (coroutineStarted)
    {
      distanceToPrey = 1000;
      StopAllCoroutines();
      coroutineStarted = false;
      StartCoroutine(UpdatePath(fleeTarget));
    }
  }

  bool IsFleeing()
  {
    // player close to predator to scare off, switch to roaming afterwards
    if (fleeing)
      return true;
    else
      return false;
  }

  IEnumerator UpdatePath(Transform target)
  {
    if (Time.timeSinceLevelLoad < 0.3f)
      yield return new WaitForSeconds(0.3f);
    PathRequestManager.RequestPath(new PathRequest(transform.position, target.position, OnPathFound));

    float sqrMoveThreshold = pathUpdateMoveThreshold * pathUpdateMoveThreshold;
    Vector3 targetPosOld = target.position;

    while (true)
    {
      yield return new WaitForSeconds(minPathUpdateTime);
      if ((target.position - targetPosOld).sqrMagnitude > sqrMoveThreshold)
      {
        PathRequestManager.RequestPath(new PathRequest(transform.position, target.position, OnPathFound));
        targetPosOld = target.position;
      }
    }
  }

  public void OnPathFound(Vector3[] waypoints, bool pathSuccess)
  {
    if (pathSuccess)
    {
      path = new Path(waypoints, transform.position, turnDistance, stoppingDistance);
      StopCoroutine("FollowPath");
      StartCoroutine("FollowPath");
    }
  }

  IEnumerator FollowPath()
  {
    bool followingPath = true;
    int pathIndex = 0;
    transform.LookAt(path.lookPoints[0]);

    float speedPercent = 1;

    while (followingPath)
    {
      Vector2 pos2D = new Vector2(transform.position.x, transform.position.z);
      while (path.turnBoundries[pathIndex].HasCrossedLine(pos2D))
      {
        if (pathIndex == path.finishLineIndex)
        {
          followingPath = false;
          break;
        }
        else
          pathIndex++;
      }

      if (followingPath)
      {
        if (pathIndex >= path.slowDownIndex && stoppingDistance > 0)
        {
          speedPercent = Mathf.Clamp01(path.turnBoundries[path.finishLineIndex].DistanceFromPoint(pos2D) / stoppingDistance);
          if (speedPercent < 0.01f)
            followingPath = false;
        }

        Quaternion targetRotation = Quaternion.LookRotation(path.lookPoints[pathIndex] - transform.position);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * turnSpeed);
        transform.Translate(Vector3.forward * Time.deltaTime * speed * speedPercent, Space.Self);
      }

      yield return null;
    }
  }

  private void OnTriggerEnter(Collider other)
  {
    if (other.tag == "Player")
    {
      StopAllCoroutines();
      chasing = false;
      Vector3 heading = other.gameObject.transform.position - transform.position;
      float distance = heading.magnitude;
      Vector3 directionToPlayer = heading / distance;
      fleeTarget.position = -directionToPlayer * Random.Range(levelLimit.x / 2, levelLimit.x);
      while (!levelGrid.NodeFromWorldPoint(fleeTarget.position).walkable) //avoid placing on obstacles
        fleeTarget.position = -directionToPlayer * Random.Range(levelLimit.x / 2, levelLimit.x);

      fleeTarget.position = new Vector3(fleeTarget.position.x, 0, fleeTarget.position.z); //make sure its on the ground

      // make sure it doesn't exceed the level
      if (fleeTarget.position.x >= levelLimit.x)
        fleeTarget.position = new Vector3(levelLimit.x - 2, fleeTarget.position.y, fleeTarget.position.z);
      else if (fleeTarget.position.x <= -levelLimit.x)
        fleeTarget.position = new Vector3(-levelLimit.x + 2, fleeTarget.position.y, fleeTarget.position.z);
      else if (fleeTarget.position.z >= levelLimit.y)
        fleeTarget.position = new Vector3(fleeTarget.position.x, fleeTarget.position.y, levelLimit.y - 2);
      else if (fleeTarget.position.z <= -levelLimit.y)
        fleeTarget.position = new Vector3(fleeTarget.position.x, fleeTarget.position.y, -levelLimit.y + 2);

      fleeing = true;
      coroutineStarted = true;
    }

    if (other.name == "FleeTarget")
    {
      if (fleeing)
      {
        fleeing = false;
        roaming = true;
      }

      if (roaming)
        RandomTarget();

      coroutineStarted = true;
    }
  }

  private void OnCollisionEnter(Collision collision)
  {
    if (collision.collider.tag == "Sheep")
    {
      scoreManager.minusScore();
      scoreManager.SetScoreText();
      scoreManager.minusSheep();
      // Teleport to Death Area
      collision.gameObject.GetComponent<SheepBehavior>().loose = true;
    }

    if (collision.collider.tag == "Pig")
    {
      scoreManager.minusScorePig();
      scoreManager.SetScoreText();
      scoreManager.minusPig();
      // Teleport to Death Area
      collision.gameObject.GetComponent<PigBehavior>().loose = true;
    }

  }

  void RandomTarget()
  {
    fleeTarget.position = new Vector3(Random.Range(-levelLimit.x + 2, levelLimit.x - 2), fleeTarget.position.y, Random.Range(-levelLimit.y + 2, levelLimit.y - 2));
    while (!levelGrid.NodeFromWorldPoint(fleeTarget.position).walkable) //avoid placing on obstacles
      fleeTarget.position = new Vector3(Random.Range(-levelLimit.x + 2, levelLimit.x - 2), fleeTarget.position.y, Random.Range(-levelLimit.y + 2, levelLimit.y - 2));
  }

  void GetClosestPrey()
  {
    // find closest sheep in array
    for (int i = 0; i < FlockManager.allFlock.Length; i++)
    {
      Vector3 direction;
      float currDistance;
      direction = FlockManager.allFlock[i].transform.position - transform.position;
      currDistance = direction.magnitude;
      if (currDistance < distanceToPrey)
      {
        distanceToPrey = currDistance;
        preyTarget = FlockManager.allFlock[i].transform;
      }
    }

    // find closest sheep in array
    for (int i = 0; i < SwineManager.allSwine.Length; i++)
    {
      Vector3 directionPig;
      float currDistancePig;
      directionPig = SwineManager.allSwine[i].transform.position - transform.position;
      currDistancePig = directionPig.magnitude;
      if (currDistancePig < distanceToPrey)
      {
        distanceToPrey = currDistancePig;
        preyTargetPig = FlockManager.allFlock[i].transform;
      }
    }
    }
}
