﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path
{
  public readonly Vector3[] lookPoints;
  public readonly Line[] turnBoundries;
  public readonly int finishLineIndex;
  public readonly int slowDownIndex;

  public Path(Vector3[] waypoints, Vector3 startPos, float turnDistance, float stoppingDistance)
  {
    lookPoints = waypoints;
    turnBoundries = new Line[lookPoints.Length];
    finishLineIndex = turnBoundries.Length - 1;

    Vector2 previousPoint = V3ToV2(startPos);
    for (int i = 0; i < lookPoints.Length; i++)
    {
      Vector2 currentPoint = V3ToV2(lookPoints[i]);
      Vector2 directionToCurrentPoint = (currentPoint - previousPoint).normalized;
      Vector2 turnBoundaryPoint = (i == finishLineIndex) ? currentPoint : currentPoint - directionToCurrentPoint * turnDistance;
      turnBoundries[i] = new Line(turnBoundaryPoint, previousPoint - directionToCurrentPoint * turnDistance);
      previousPoint = turnBoundaryPoint;
    }

    float distFromEndPoint = 0;
    for (int i = lookPoints.Length - 1; i > 0; i--)
    {
      distFromEndPoint += Vector3.Distance(lookPoints[i], lookPoints[i - 1]);
      if (distFromEndPoint > stoppingDistance)
      {
        slowDownIndex = i;
        break;
      }
    }
  }

  Vector2 V3ToV2(Vector3 v3)
  {
    return new Vector2(v3.x, v3.z);
  }
}
