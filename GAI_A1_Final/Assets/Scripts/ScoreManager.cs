﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
  public Text scoreText;

  public int score;
  public int scoreValue;
  public int scoreValuePig;
  public float initialSheep;
  public float sheepLeft;
  public float initialPigs;
  public float pigsLeft;
  [SerializeField]
  float timeLeft;
  public Text timeText;
  public Text endText;
  public static bool end;
  private bool displayOnce;

  public GameObject endMenuUI;


  private void Start()
  {
    Time.timeScale = 1f;
    score = 0;
    scoreValue = 5;
    scoreValuePig = 20;
    timeLeft = 360.0f;
    initialSheep = FlockManager.flockSize;
    sheepLeft = FlockManager.flockSize;
    initialPigs = SwineManager.swineSize;
    pigsLeft = SwineManager.swineSize;
    end = false;
    displayOnce = true;
    SetScoreText();
  }

  private void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.CompareTag("Sheep"))
    {
      //Destroy(other.gameObject);
      score += scoreValue;
      SetScoreText();
      minusSheep();
      //Teleport Sheep
      other.gameObject.GetComponent<SheepBehavior>().win = true;
    }

    if (other.gameObject.CompareTag("Pig"))
    {
        //Destroy(other.gameObject);
        score += scoreValuePig;
        SetScoreText();
        minusPig();
        //Teleport Sheep
        other.gameObject.GetComponent<PigBehavior>().win = true;
    }
    }

  public void minusScore()
  {
    score -= scoreValue;
  }

public void minusScorePig()
{
    score -= (scoreValuePig/2);
}

    public void minusSheep()
  {
    sheepLeft -= 1;
  }

  public void minusPig()
  {
      pigsLeft -= 1;
  }

    public void SetScoreText()
  {
    scoreText.text = "Score: " + score.ToString();
  }

  void SetTimeText()
  {
    timeText.text = "Time Left: " + Mathf.Round(timeLeft).ToString(); ;
  }

  void Update()
  {
    if (timeLeft >= 0)
    {
      timeLeft -= Time.deltaTime;
    }

    if (timeLeft <= 0f || sheepLeft <= 0 || pigsLeft <= 0)
    {
      end = true;
      if (displayOnce)
      {
        displayOnce = false;
        endGame();
        winCheck();
      }

    }
    else
    {
      SetTimeText();
    }
  }

  void winCheck()
  {
    //>80%
    if (score >= (((initialSheep * .8) * scoreValue) + ((initialPigs * .8) * scoreValuePig)))
    {
      endText.text = "Congratulations! You guided the number of animals needed for a successful breeding season! You win with a score of " + score.ToString();
    }
    //<20%
    else if (score < ((initialSheep * .2) * scoreValue) + ((initialPigs * .2) * scoreValuePig))
    {
      endText.text = "Sorry! You didn't escort enough animals to the pen for the breeding season, better luck next time!";
    }
    //All Sheep Gone
    else if (sheepLeft <= 0 && pigsLeft <= 0)
    {
      //>80%
      if (score >= ((initialSheep * .8) * scoreValue) + ((initialPigs * .8) * scoreValuePig))
      {
        endText.text = "Congratulations! You guided the number of animals needed for a successful breeding season! You win with a score of " + score.ToString();
      }
      //>65%
      else if (score >= ((initialSheep * .65) * scoreValue) + ((initialPigs * .65) * scoreValuePig))
      {
        endText.text = "Well done! You guided just enough animals needed for a good breeding season! You win with a score of " + score.ToString();
      }
      //<65%
      else if (score < ((initialSheep * .65) * scoreValue) + ((initialPigs * .65) * scoreValuePig))
      {
        endText.text = "Sorry! You didn't escort enough animals to the pen for the breeding season, better luck next time!";
      }
    }
  }

  void endGame()
  {
    Time.timeScale = 0f;
    endMenuUI.SetActive(true);
  }
}
