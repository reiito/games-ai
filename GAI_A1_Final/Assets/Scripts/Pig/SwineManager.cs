﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwineManager : MonoBehaviour
{

  public static int swineSize = 16;
  public GameObject pigPrefab;
  public GameObject pigSpawn;
  public static GameObject[] allSwine = new GameObject[swineSize];
  public static int startRoom = 30;

  // Use this for initialization
  void Start()
  {
    int i = 0;
    for (int x = 0; x < Mathf.Sqrt(swineSize); x++)
    {
      for (int y = 0; y < Mathf.Sqrt(swineSize); y++)
      {
        Vector3 pos = new Vector3(pigSpawn.transform.position.x + x * 2.0f,
                        0,
                        pigSpawn.transform.position.z + y * 2.0f);
        allSwine[i] = (GameObject)Instantiate(pigPrefab, pos, Quaternion.identity, transform);
        allSwine[i].AddComponent<Rigidbody>();
        allSwine[i].AddComponent<BoxCollider>();
        allSwine[i].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY;
        allSwine[i].GetComponent<Rigidbody>().isKinematic = true;
        i++;
      }
    }
  }

}
