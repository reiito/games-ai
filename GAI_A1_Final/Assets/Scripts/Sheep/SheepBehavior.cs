﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepBehavior : StateMachine
{

  public float speed = 0f;

  // Maximal speed of the flock so the sheperd can catch up
  public float maxSpeed = 20.0f;

  public float rotationSpeed = 7.0f;

  // Max distance to group with anothersheep
  public float scanDistance = 6.0f;

  // Avoid distance with other sheeps to avoid collision
  public float minDistSide = 1.0f;
  public float minDistFront = 1.0f;

  // Sheperd flee distance
  public float fleeDist = 3.0f;

  public float groupSpeedBase = 0.0f;

  // Direction to flee from the sheperd
  public Vector3 fleeingGoal = Vector3.zero;

  // My goal forward + avoid + centre
  public Vector3 myGoal = Vector3.zero;

  public bool flee = false;

  public bool win = false;
  public bool loose = false;

  void Start()
  {
    SetDefaultstate(Idle);
    AddStateAndCondition(FleeState, FleeingBehavior);
    AddStateAndCondition(FlockBehavior, FlockCondition);
    AddStateAndCondition(AvoidObstacleState, AvoidObstacleCondition);
    AddStateAndCondition(Winning, IsWinning);
    AddStateAndCondition(Loosing, IsLoosing);
    myGoal = this.transform.position + transform.forward * 50;
  }


  bool IsWinning()
  {
    return win;
  }

  bool IsLoosing()
  {
    return loose;
  }

  void Winning()
  {

    this.transform.position = GameObject.Find("WinSpot").transform.position;
  }

  void Loosing()
  {
    this.transform.position = GameObject.Find("LoseSpot").transform.position;
  }

  bool AvoidObstacleCondition()
  {
    Vector3 fwd = transform.TransformDirection(Vector3.forward);
    Vector3 left = Quaternion.AngleAxis(15, transform.up) * fwd;
    Vector3 right = Quaternion.AngleAxis(-15, transform.up) * fwd;
    RaycastHit hit;

    if (Physics.Raycast(transform.position, fwd, out hit, minDistFront) ||
        Physics.Raycast(transform.position, right, out hit, minDistFront) ||
        Physics.Raycast(transform.position, left, out hit, minDistFront))
    {
      if (hit.transform.tag == "Obstacle")
        return true;
    }
    return false;
  }

  void AvoidObstacleState()
  {
    Vector3 fwd = transform.TransformDirection(Vector3.forward);

    Vector3 dir = Quaternion.AngleAxis(15, transform.up) * fwd;
    transform.rotation = Quaternion.Slerp(transform.rotation,
      Quaternion.LookRotation(dir),
      rotationSpeed * Time.deltaTime);
  }

  void FleeState()
  {
    transform.rotation = Quaternion.Slerp(transform.rotation,
      Quaternion.LookRotation(fleeingGoal),
      rotationSpeed * Time.deltaTime);
    speed = 0.8f;
    groupSpeedBase = 0.08f;
    transform.Translate(0, 0, Time.deltaTime * speed);
  }

  bool FleeingBehavior()
  {
    Vector3 sheperdPos = PlayerMove.myPos;
    Vector3 wolfPos = GameObject.Find("predator(Clone)").transform.position;

    float dist = Vector3.Distance(this.transform.position, sheperdPos);
    float distWolf = Vector3.Distance(this.transform.position, wolfPos);
    if (dist < fleeDist || distWolf < fleeDist)
    {
      flee = true;
      if (dist < fleeDist)
        fleeingGoal = Vector3.zero + (this.transform.position - sheperdPos);
      else if (distWolf < fleeDist)
        fleeingGoal = Vector3.zero + (this.transform.position - wolfPos);

      speed = 0.8f;

      if (dist < fleeDist * 0.5f)
        speed = 1.6f;

      myGoal = this.transform.position + transform.forward * 50;
      return true;
    }
    flee = false;
    return false;
  }

  bool FlockCondition()
  {
    if (flee == false)
    {
      if (Random.Range(0, 5) != 0)
        return true;
    }
    return false;
  }

  void FlockBehavior()
  {
    GameObject[] myFlock = FlockManager.allFlock;

    Vector3 center = Vector3.zero;
    Vector3 avoid = Vector3.zero;

    float gSpeed = groupSpeedBase;
    float dist;
    int groupSize = 0;
    Vector3 avgGoal = Vector3.zero;

    foreach (GameObject current in myFlock)
    {
      if (current != this.gameObject)
      {
        dist = Vector3.Distance(current.transform.position, this.transform.position);

        if (dist <= scanDistance)
        {

          center += current.transform.position;
          groupSize++;
          SheepBehavior otherSheep = current.GetComponent<SheepBehavior>();
          avgGoal += otherSheep.myGoal;

          if (dist < minDistSide)
          {
            avoid = avoid + (this.transform.position - current.transform.position);
          }

          gSpeed = gSpeed + otherSheep.speed;
        }
      }
    }

    if (groupSize > 0)
    {
      avgGoal = avgGoal / groupSize;
      center = center / groupSize + (avgGoal - this.transform.position);
      speed = gSpeed / groupSize;
      if (speed > maxSpeed)
        speed = maxSpeed;
      Vector3 direction = (center + avoid) - transform.position;
      if (direction.y != 0)
        direction.y = 0;
      Debug.DrawLine(transform.position, myGoal, Color.red);
      if (direction != Vector3.zero)
      {
        transform.rotation = Quaternion.Slerp(transform.rotation,
          Quaternion.LookRotation(direction),
          rotationSpeed * Time.deltaTime * 2);
      }
    }

    Vector3 fwd = transform.TransformDirection(Vector3.forward);
    Vector3 left = Quaternion.AngleAxis(15, transform.up) * fwd;
    Vector3 right = Quaternion.AngleAxis(-15, transform.up) * fwd;
    RaycastHit hit;

    if ((Physics.Raycast(transform.position, fwd, out hit, minDistFront) ||
        Physics.Raycast(transform.position, right, out hit, minDistFront) ||
        Physics.Raycast(transform.position, left, out hit, minDistFront)) && (hit.transform.tag == "sheep"))
    {
    }
    else if ((Physics.Raycast(transform.position, fwd, out hit, minDistFront + 10) ||
             Physics.Raycast(transform.position, right, out hit, minDistFront + 10) ||
             Physics.Raycast(transform.position, left, out hit, minDistFront + 10)) && (hit.transform.tag == "sheep"))
    {
      //	this.GetComponent<Rigidbody>().AddForce(transform.forward * 15.0f);
      transform.Translate(0, 0, Time.deltaTime * speed * 1.5f);
    }
    else
    {
      //	this.GetComponent<Rigidbody>().AddForce(transform.forward * 15.0f);
      transform.Translate(0, 0, Time.deltaTime * speed);
    }


    myGoal = this.transform.position + transform.forward * 50;

  }

  void Idle()
  {
  }

}